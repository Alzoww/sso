package tests

import (
	"github.com/brianvoe/gofakeit/v6"
	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	ssov1 "gitlab.com/Alzoww/protos/gen/go/sso"
	"gitlab.com/Alzoww/sso/tests/suite"
	"google.golang.org/grpc/status"
	"testing"
	"time"
)

const (
	emptyAppId = 0
	appID      = 1
	appSecret  = "test-secret"

	passDefaultLen = 10
)

func TestRegisterLogin_HappyPath(t *testing.T) {
	ctx, st := suite.New(t)

	email := gofakeit.Email()
	password := gofakeit.Password(true, true, true, true, false, passDefaultLen)

	respReg, err := st.AuthClient.Register(ctx, &ssov1.RegisterRequest{Email: email, Password: password})
	require.NoError(t, err)
	assert.NotEmpty(t, respReg.GetUserId())

	respLogin, err := st.AuthClient.Login(ctx, &ssov1.LoginRequest{Email: email, Password: password, AppId: appID})
	require.NoError(t, err)
	loginTime := time.Now()

	token := respLogin.GetToken()
	require.NotEmpty(t, token)

	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(appSecret), nil
	})
	require.NoError(t, err)

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	require.True(t, ok)

	assert.Equal(t, respReg.GetUserId(), int64(claims["uid"].(float64)))
	assert.Equal(t, email, claims["email"].(string))
	assert.Equal(t, appID, int(claims["app_id"].(float64)))

	const deltaSec = 1
	assert.InDelta(t, loginTime.Add(st.Cfg.TokenTTL).Unix(), int64(claims["exp"].(float64)), deltaSec)
}

func TestRegister_BadRequests(t *testing.T) {
	tests := []struct {
		name        string
		req         *ssov1.RegisterRequest
		wantMessage string
	}{
		{"Register with empty email", &ssov1.RegisterRequest{Email: "", Password: "test-pass"}, "missing email"},

		{"Register with empty password", &ssov1.RegisterRequest{Email: "test-email", Password: ""}, "missing password"},

		{"Register with both empty", &ssov1.RegisterRequest{Email: "", Password: ""}, "missing email"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx, st := suite.New(t)

			respReg, err := st.AuthClient.Register(ctx, tt.req)
			assert.Error(t, err)
			assert.Empty(t, respReg.GetUserId())

			errStatus, ok := status.FromError(err)
			assert.True(t, ok)
			assert.Equal(t, tt.wantMessage, errStatus.Message())
		})
	}
}

func TestLogin_BadRequests(t *testing.T) {
	ctx, st := suite.New(t)

	tests := []struct {
		name        string
		req         *ssov1.LoginRequest
		wantMessage string
	}{
		{"Login with empty email", &ssov1.LoginRequest{Email: "", Password: "test-pass", AppId: int32(1)}, "missing email"},

		{"Login with empty password", &ssov1.LoginRequest{Email: "test-email", Password: "", AppId: int32(1)}, "missing password"},

		{"Login with empty app id", &ssov1.LoginRequest{Email: "test-email", Password: "test-pass"}, "missing app id"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			respReg, err := st.AuthClient.Login(ctx, tt.req)
			assert.Error(t, err)
			assert.Equal(t, "", respReg.GetToken())

			errStatus, ok := status.FromError(err)
			assert.True(t, ok)
			assert.Equal(t, tt.wantMessage, errStatus.Message())
		})
	}
}

func TestRegister_DuplicatedRegistration(t *testing.T) {
	ctx, st := suite.New(t)

	email := gofakeit.Email()
	password := gofakeit.Password(true, true, true, true, false, passDefaultLen)

	respReg, err := st.AuthClient.Register(ctx, &ssov1.RegisterRequest{Email: email, Password: password})
	require.NoError(t, err)
	assert.NotEmpty(t, respReg.GetUserId())

	respReg, err = st.AuthClient.Register(ctx, &ssov1.RegisterRequest{Email: email, Password: password})
	require.Error(t, err)
	assert.Empty(t, respReg.GetUserId())
	assert.ErrorContains(t, err, "user already exists")
}
