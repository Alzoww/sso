package sqlite

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/mattn/go-sqlite3"
	"gitlab.com/Alzoww/sso/internal/domain"
	"gitlab.com/Alzoww/sso/internal/storage"
)

type Storage struct {
	db *sql.DB
}

func New(storagePath string) (*Storage, error) {
	const f = "storage.sqlite.New"

	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", f, err)
	}

	return &Storage{db: db}, nil
}

func (s *Storage) SaveUser(ctx context.Context, email string, passHash []byte) (uid int64, err error) {
	const f = "storage.sqlite.SaveUser"

	stmt, err := s.db.Prepare("INSERT INTO users(email, pass_hash) VALUES (?, ?)")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", f, err)
	}
	defer stmt.Close()

	res, err := stmt.ExecContext(ctx, email, passHash)
	if err != nil {
		var sqliteErr sqlite3.Error

		if errors.As(err, &sqliteErr) && errors.Is(sqliteErr.ExtendedCode, sqlite3.ErrConstraintUnique) {
			return 0, fmt.Errorf("%s: %w", f, storage.ErrUserExists)
		}

		return 0, fmt.Errorf("%s: %w", f, err)
	}

	uid, err = res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("%s: %w", f, err)
	}

	return uid, nil
}

func (s *Storage) User(ctx context.Context, email string) (user domain.User, err error) {
	const f = "storage.sqlite.User"

	stmt, err := s.db.Prepare("SELECT id, email, pass_hash FROM users WHERE email = ?")
	if err != nil {
		return domain.User{}, fmt.Errorf("%s: %w", f, err)
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx, email)

	err = row.Scan(&user.ID, &user.Email, &user.PassHash)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return domain.User{}, fmt.Errorf("%s: %w", f, storage.ErrUserNotFound)
		}

		return domain.User{}, fmt.Errorf("%s: %w", f, err)
	}

	return user, nil
}

func (s *Storage) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	const f = "storage.sqlite.IsAdmin"

	stmt, err := s.db.Prepare("SELECT is_admin FROM users WHERE id = ?")
	if err != nil {
		return false, fmt.Errorf("%s: %w", f, err)
	}
	defer stmt.Close()

	var isAdmin bool
	err = stmt.QueryRowContext(ctx, userID).Scan(&isAdmin)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, fmt.Errorf("%s: %w", f, storage.ErrUserNotFound)
		}

		return false, fmt.Errorf("%s: %w", f, err)
	}

	return isAdmin, nil
}

func (s *Storage) App(ctx context.Context, appID int) (app domain.App, err error) {
	const f = "storage.sqlite.App"

	stmt, err := s.db.Prepare("SELECT * FROM apps WHERE id = ?")
	if err != nil {
		return domain.App{}, fmt.Errorf("%s: %w", f, err)
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx, appID)

	err = row.Scan(&app.ID, &app.Name, &app.Secret)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return domain.App{}, fmt.Errorf("%s: %w", f, storage.ErrAppNotFound)
		}

		return domain.App{}, fmt.Errorf("%s: %w", f, err)
	}

	return app, nil
}
