package grpcapp

import (
	"fmt"
	authgrpc "gitlab.com/Alzoww/sso/internal/grpc/auth"
	"google.golang.org/grpc"
	"log/slog"
	"net"
)

type App struct {
	log        *slog.Logger
	gRPCServer *grpc.Server
	port       int
}

// New creates new grpc server app
func New(log *slog.Logger, authService authgrpc.AuthI, port int) *App {
	gRPCServer := grpc.NewServer()

	authgrpc.Register(gRPCServer, authService)

	return &App{
		log:        log,
		gRPCServer: gRPCServer,
		port:       port,
	}
}

func (a *App) MustRun() {
	err := a.Run()

	if err != nil {
		panic(err)
	}
}

func (a *App) Run() error {
	const f = "grpcapp.Run"

	log := a.log.With(
		slog.String("function", f),
		slog.Int("port", a.port),
	)

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", a.port))
	if err != nil {
		return fmt.Errorf("%s: %w", f, err)
	}

	log.Info("grpc server started", slog.String("address", l.Addr().String()))

	err = a.gRPCServer.Serve(l)
	if err != nil {
		return fmt.Errorf("%s: %w", f, err)
	}

	return nil
}

func (a *App) Stop() {
	const f = "grpcapp.Stop"

	a.log.With(slog.String("function", f)).Info("grpc server stopped", slog.Int("port", a.port))

	a.gRPCServer.GracefulStop()
}
