package auth

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/Alzoww/sso/internal/domain"
	"gitlab.com/Alzoww/sso/internal/storage"
	"gitlab.com/Alzoww/sso/pkg/jwt"
	"gitlab.com/Alzoww/sso/pkg/logger/sl"
	"golang.org/x/crypto/bcrypt"
	"log/slog"
	"time"
)

type Auth struct {
	log          *slog.Logger
	userSaver    UserSaver
	userProvider UserProvider
	appProvider  AppProvider
	tokenTTL     time.Duration
}

type UserSaver interface {
	SaveUser(ctx context.Context, email string, passHash []byte) (uid int64, err error)
}

type UserProvider interface {
	User(ctx context.Context, email string) (user domain.User, err error)
	IsAdmin(ctx context.Context, userID int64) (bool, error)
}

type AppProvider interface {
	App(ctx context.Context, appID int) (app domain.App, err error)
}

func New(log *slog.Logger, userSaver UserSaver, userProvider UserProvider, appProvider AppProvider, tokenTTL time.Duration) *Auth {
	return &Auth{
		log:          log,
		userSaver:    userSaver,
		userProvider: userProvider,
		appProvider:  appProvider,
		tokenTTL:     tokenTTL,
	}
}

func (a *Auth) Login(ctx context.Context, email, password string, appID int) (token string, err error) {
	const f = "auth.Login"

	log := a.log.With(
		slog.String("function", f),
		slog.String("email", email),
	)

	user, err := a.userProvider.User(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			log.Warn("user not found")

			return "", fmt.Errorf("%s: %w", f, err)
		}

		log.Error("failed to get user", sl.Err(err))

		return "", fmt.Errorf("%s: %w", f, err)
	}

	err = bcrypt.CompareHashAndPassword(user.PassHash, []byte(password))
	if err != nil {
		log.Error("invalid credentials", sl.Err(err))

		return "", fmt.Errorf("%s: %w", f, errors.New("invalid credentials"))
	}

	app, err := a.appProvider.App(ctx, appID)
	if err != nil {
		return "", fmt.Errorf("%s: %w", f, err)
	}

	log.Info("user logged in successfully")

	token, err = jwt.NewToken(user, app, a.tokenTTL)
	if err != nil {
		log.Error("failed to create token", sl.Err(err))

		return "", fmt.Errorf("%s: %w", f, err)
	}

	return token, nil
}

func (a *Auth) RegisterNewUser(ctx context.Context, email, password string) (userID int64, err error) {
	const f = "auth.registerNewUser"

	log := a.log.With(
		slog.String("function", f),
		slog.String("email", email),
	)

	log.Info("register new user")

	passHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("failed to generate hash password", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", f, err)
	}

	id, err := a.userSaver.SaveUser(ctx, email, passHash)
	if err != nil {
		log.Error("failed to save user", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", f, err)
	}

	log.Info("user registered successfully")

	return id, nil
}

func (a *Auth) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	const f = "auth.isAdmin"

	log := a.log.With(
		slog.String("function", f),
		slog.Int64("user_id", userID),
	)

	log.Info("checking if user is admin")

	isAdmin, err := a.userProvider.IsAdmin(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrAppNotFound) {
			log.Warn("app not found")
			return false, fmt.Errorf("%s: %w", f, errors.New("app not found"))
		}
		log.Error("failed to check if user is admin", sl.Err(err))

		return false, fmt.Errorf("%s: %w", f, err)
	}

	log.Info("user is admin")

	return isAdmin, nil
}
